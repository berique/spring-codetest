# Sugestões de arquitetura

## Ter dois arquivos do docker-compose

Básicamente o objetivo seria ter dois arquivos do docker-compose:

* docker-compose.yml
* docker-compose.dev.yml

Aonde o docker-compose.dev.yml seria utilizado em desenvolvimento.

## Usar MailHog em desenvolvimento

Utilizando a imagem ```mailhog/mailhog``` e apontando o SMTP para ```mail:1025``` simplificaria para o desenvolvedor, pois
ele funciona como um servidor de e-mail fake e possui uma interface parecida com o Gmail em `localhost:8025` que verifica como o e-mail está sendo enviado.

Além disso também evita certo constrangimentos de um e-mail ser enviado para o e-mail de terceiros.


## Ter um application.properties externalizado em produção

Criar um arquivo application.properties que seja versionado em um outro repositório que será chamado "mais ou menos" assim:

```java -jar /opt/spring-codetest.jar --spring.config.location=classpath:/application.properties,file:/opt/producao.properties```

No arquivo ```producao.properties``` algumas propriedades serão sobrescritas ou adicionadas, tais como senha de segurança, SMTP e etc. 

## Debug no docker-compose de desenvolvimento

Adicionando `java -agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n -Djava.security.egd=file:/dev/./urandom -jar /opt/spring-codetest.jar` no `docker-compose.dev.yml` é possivel fazer debug remoto.