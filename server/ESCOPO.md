# CODE TEST - RED ​SPARK

## ESCOPO

Uma clínica veterinária gostaria de cadastrar todos os animais que são tratados por eles,
registrando cada consulta efetuada pelos veterinários e as vacinas de cada um de seus
animais, além de deixar pré agendado as futuras vacinas para notificar seus clientes por
e-mail quando a data estiver próxima.
É importante sempre registrar o veterinário responsável por cada consulta

### O que será avaliado

* Mapeamento das entidades (Back End)
* A lógica utilizada para solução do problema
* Agilidade na resolução
* Estrutura do código
* Testes unitários / Testes de integração