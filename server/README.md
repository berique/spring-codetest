# Leia-me

Basicamente temos 3 documentos distintos:

* [Escopo](ESCOPO.md) - Escopo do projeto.
* [Solução](SOLUCAO.md) - Descrição de qual foi a solução encontrada.
* [Sugestões](SUGESTOES.md) - Sugestões de arquitetura que podem ser uteis para o desenvolvimento e arquitetura.
