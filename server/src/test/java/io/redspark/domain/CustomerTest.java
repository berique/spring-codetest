package io.redspark.domain;

import io.redspark.security.Roles;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CustomerTest {

    @Test
    public void custumerWithValidRole() {
        Client customer = new Client();
        assertThat(customer.getRole(), is(Roles.ROLE_CLIENT));
    }
}