package io.redspark.repository;

import io.redspark.ApplicationTest;
import io.redspark.domain.Client;
import io.redspark.domain.Pet;
import org.hamcrest.core.IsNot;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class ClientRepositoryTest extends ApplicationTest {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private PetRepository petRepository;

    @Test
    public void insertClientWithoutPet() {
        assertThat(clientRepository.count(), is(0L));
        clientRepository.save(Client.builder().name("john").login("arbacle").password("wayne").email("john@abacle.wayne").build());
        assertThat(clientRepository.count(), is(1L));
    }

    @Test
    public void insertClientWithoutPet2() {
        insertClientWithoutPet();
        clientRepository.save(Client.builder().name("john2").login("arbacle2").password("wayne").email("john@wayne").build());
        assertThat(clientRepository.count(), is(2L));
    }

    @Test(expected = JpaSystemException.class)
    public void insertDuplicatedEmail() {
        insertClientWithoutPet2();
        clientRepository.save(Client.builder().name("john3").login("arbacle3").password("wayne").email("john@abacle.wayne").build());
    }

    @Test
    public void updateClientWithoutPet() {
        insertClientWithoutPet();

        Client result = clientRepository.findAll().get(0);
        result.setName("Ringo");
        clientRepository.save(result);
        assertThat(clientRepository.count(), is(1L));
        assertThat(clientRepository.findOne(result.getId()).getName(), is("Ringo"));
    }

    @Test
    public void insertClientWithPets() {
        assertThat(clientRepository.count(), is(0L));
        final Client build = Client.builder().name("john").login("arbacle").password("wayne").build();
        build.setPets(Arrays.asList(
                new Pet[]{
                        Pet.builder().client(build).name("f").build()
                }
        ));
        Client result = clientRepository.save(build);
        assertThat(result.getId(), is(IsNull.notNullValue()));
        assertThat(result.getId(), IsNot.not(0L));
        assertThat(result.getPets().get(0).getClient().getId(), is(result.getId()));
        assertThat(clientRepository.count(), is(1L));
        assertThat(petRepository.count(), is(1L));
    }

    @Test
    public void updateClientWithPet() {
        insertClientWithPets();

        Client result = clientRepository.findAll().get(0);

        assertThat(result.getPets(), is(IsNull.notNullValue()));
        assertThat(result.getPets().size(), is(1));

        result.setName("Ringo");
        clientRepository.save(result);

        assertThat(clientRepository.count(), is(1L));
        assertThat(clientRepository.findOne(result.getId()).getName(), is("Ringo"));
    }

    @Test
    public void deleteClientWithoutPet() {
        insertClientWithoutPet();

        Client result = clientRepository.findAll().get(0);
        clientRepository.delete(result.getId());
        assertThat(clientRepository.count(), is(0L));
    }

    @Test
    public void deleteClientWithPet() {
        insertClientWithPets();

        Client result = clientRepository.findAll().get(0);
        clientRepository.delete(result.getId());
        assertThat(clientRepository.count(), is(0L));
        assertThat(petRepository.count(), is(0L));
    }

}