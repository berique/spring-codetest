package io.redspark.repository;

import io.redspark.ApplicationTest;
import io.redspark.domain.Appointment;
import io.redspark.domain.Client;
import io.redspark.domain.Vet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class AppointmentRepositoryTest extends ApplicationTest {

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private VetRepository vetRepository;

    @Test
    public void insert() {
        Client client = clientRepository.save(VaccinationRepositoryTest.createClient());

        Vet vet = vetRepository.save(Vet.builder().name("Vet").login("vet").password("vet").build());

        assertThat(appointmentRepository.save(Appointment.builder().pet(client.getPets().get(0)).vet(vet).build()), is(notNullValue()));
    }

}