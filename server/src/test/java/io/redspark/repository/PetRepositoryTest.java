package io.redspark.repository;

import io.redspark.ApplicationTest;
import io.redspark.domain.Client;
import io.redspark.domain.Pet;
import io.redspark.domain.Vaccination;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class PetRepositoryTest extends ApplicationTest {

    @Autowired
    private PetRepository petRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private VaccinationRepository vaccinationRepository;

    @Test(expected = DataIntegrityViolationException.class)
    public void insertPetWithoutClient() {
        Pet pet = Pet.builder().name("Flufy").build();
        petRepository.save(pet);
    }

    @Test
    public void insertPetWithClient() {

        Client client = clientRepository.save(Client.builder()
                .name("Michael Scott")
                .login("michaell")
                .password("scott")
                .email("michael.scott@dundermiflin.com")
                .build());

        Pet pet = Pet.builder()
                .name("Flufy")
                .client(client)
                .build();

        petRepository.save(pet);

        assertThat(petRepository.count(), is(1L));
        assertThat(clientRepository.count(), is(1L));
    }

    @Test
    public void updatePetWithClient() {
        insertPetWithClient();

        Pet pet = petRepository.findAll().get(0);
        pet.setName("Scooby-doo");

        petRepository.save(pet);

        assertThat(petRepository.findAll().get(0).getName(), is("Scooby-doo"));

    }

    @Test
    public void deletePetWithClient() {
        insertPetWithClient();

        Pet pet = petRepository.findAll().get(0);

        petRepository.delete(pet);

        assertThat(petRepository.count(), is(0L));
        assertThat(clientRepository.count(), is(1L));
    }

    @Test
    public void insertPetWithClientAndVaccine() {
        Client client = clientRepository.save(Client.builder()
                .name("Michael Scott")
                .login("michaell")
                .password("scott")
                .email("michael.scott@dundermiflin.com")
                .build());

        Pet pet = Pet.builder()
                .name("Flufy")
                .client(client)
                .build();

        pet.setVaccinations(Arrays.asList(
                new Vaccination[]{
                        Vaccination.builder().name("Teste").vaccinationDate(new Date()).pet(pet).build()
                }
        ));

        petRepository.save(pet);

        assertThat(petRepository.count(), is(1L));
        assertThat(clientRepository.count(), is(1L));
        assertThat(vaccinationRepository.findAll().get(0), is(notNullValue()));
        assertThat(vaccinationRepository.findAll().get(0).getPet(), is(notNullValue()));
        assertThat(vaccinationRepository.findAll().get(0).getPet().getId(), is(pet.getId()));
        assertThat(vaccinationRepository.count(), is(1L));
    }
}