package io.redspark.repository;

import io.redspark.ApplicationTest;
import io.redspark.domain.Client;
import io.redspark.domain.Pet;
import io.redspark.domain.Vaccination;
import io.redspark.domain.Vet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class VaccinationRepositoryTest extends ApplicationTest {

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    @Autowired
    private VaccinationRepository vaccinationRepository;
    @Autowired
    private VetRepository vetRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Test
    public void insert() {
        Client client = clientRepository.save(createClient());

        Vet vet = vetRepository.save(Vet.builder().name("Vet").login("vet").password("vet").build());

        Vaccination vaccination = Vaccination.builder()
                .vet(vet)
                .pet(client.getPets().get(0))
                .vaccinationDate(new Date()) //
                .name("XKZ") //
                .build();

        vaccinationRepository.save(vaccination);

        assertThat(vaccinationRepository.count(), is(1L));
    }

    @Test
    public void findByVaccinationDate() throws ParseException {
        Client client = clientRepository.save(createClient());

        Vet vet = vetRepository.save(Vet.builder().name("Vet").login("vet").password("vet").build());

        Vaccination vaccination = Vaccination.builder()
                .vet(vet)
                .pet(client.getPets().get(0))
                .vaccinationDate(sdf.parse("03/06/2018 14:00:00")) //
                .name("XKZ") //
                .build();

        vaccinationRepository.save(vaccination);

        vaccination = Vaccination.builder()
                .vet(vet)
                .pet(client.getPets().get(0))
                .vaccinationDate(sdf.parse("04/06/2018 12:00:00")) //
                .name("XKZ") //
                .build();

        vaccinationRepository.save(vaccination);

        vaccination = Vaccination.builder()
                .vet(vet)
                .pet(client.getPets().get(0))
                .vaccinationDate(sdf.parse("05/06/2018 12:00:00")) //
                .name("XKZ") //
                .build();

        vaccinationRepository.save(vaccination);

        assertThat(vaccinationRepository.findByVaccinationDateBetween(
                sdf.parse("04/06/2018 00:00:00"),
                sdf.parse("04/06/2018 23:59:59")).size(), is(1));

    }

    public static Client createClient() {
        Client client2 = Client.builder()
                .name("Cliente")
                .login("client") //
                .password("client") //
                .email("cli@ent") //
                .build();
        client2.setPets(
                Arrays.asList(new Pet[]{ //
                        Pet.builder().client(client2).name("fluffy").build() //
                }) //
        );
        return client2;
    }
}