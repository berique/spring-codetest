package io.redspark;

import org.junit.After;
import org.junit.Before;
import org.subethamail.wiser.Wiser;

public abstract class MailApplicationTest extends ApplicationTest {
    protected Wiser wiser;

    @Before
    public void config() {
        wiser = new Wiser(10002);
        wiser.start();
    }

    @After
    public void tearDown() throws Exception {
        wiser.stop();
    }
}
