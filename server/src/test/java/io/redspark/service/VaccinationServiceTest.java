package io.redspark.service;

import io.redspark.MailApplicationTest;
import io.redspark.domain.Client;
import io.redspark.domain.Pet;
import io.redspark.domain.Vaccination;
import io.redspark.domain.Vet;
import io.redspark.repository.ClientRepository;
import io.redspark.repository.VaccinationRepository;
import io.redspark.repository.VetRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.subethamail.wiser.WiserMessage;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class VaccinationServiceTest extends MailApplicationTest {

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    @Autowired
    private VaccinationService vaccinationService;
    @Autowired
    private VaccinationRepository vaccinationRepository;
    @Autowired
    private VetRepository vetRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Test
    public void testCheckVaccinationAndSendMail() throws ParseException, MessagingException, IOException {
        assertThat(wiser.getMessages().isEmpty(), is(true));

        Client client = clientRepository.save(createClient());

        Vet vet = vetRepository.save(Vet.builder().name("Vet").login("vet").password("vet").build());

        Vaccination vaccination = Vaccination.builder()
                .vet(vet)
                .pet(client.getPets().get(0))
                .vaccinationDate(sdf.parse("03/06/2018 14:00:00")) //
                .name("XKZ") //
                .build();

        vaccinationRepository.save(vaccination);

        vaccination = Vaccination.builder()
                .vet(vet)
                .pet(client.getPets().get(0))
                .vaccinationDate(sdf.parse("04/06/2018 12:00:00")) //
                .name("XKZ") //
                .build();

        vaccinationRepository.save(vaccination);

        vaccination = Vaccination.builder()
                .vet(vet)
                .pet(client.getPets().get(0))
                .vaccinationDate(sdf.parse("05/06/2018 12:00:00")) //
                .name("XKZ") //
                .build();

        vaccinationRepository.save(vaccination);

        vaccinationService.checkVaccinationAndSendMail(sdf.parse("04/06/2018 10:00:00"));

        assertThat(wiser.getMessages().isEmpty(), is(false));
        assertThat(wiser.getMessages().size(), is(1));
        final WiserMessage wiserMessage = wiser.getMessages().get(0);
        assertThat(wiserMessage.getEnvelopeReceiver(), is(client.getEmail()));
        assertThat(wiserMessage.getMimeMessage().getSubject(), is(vaccinationService.createSubject(vaccination)));
    }

    private Client createClient() {
        Client client2 = Client.builder()
                .name("Cliente")
                .login("client") //
                .password("client") //
                .email("cli@ent") //
                .build();
        client2.setPets(
                Arrays.asList(new Pet[]{ //
                        Pet.builder().client(client2).name("fluffy").build() //
                }) //
        );
        return client2;
    }


}