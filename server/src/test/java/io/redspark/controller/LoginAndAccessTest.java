package io.redspark.controller;

import io.redspark.ApplicationTest;
import io.redspark.controller.dto.UserDTO;
import io.redspark.domain.User;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static io.redspark.compose.Compose.admin;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class LoginAndAccessTest extends ApplicationTest {

  @Test
  public void testLogin() throws Exception {
    User user = admin("bruno");
    saveall(user);

    ResponseEntity<UserDTO> response = post("/login").formParam("username", user.getLogin())
        .formParam("password", user.getPassword()).getResponse(UserDTO.class);

    assertThat(response.getStatusCode(), is(HttpStatus.OK));
    assertThat(response.getBody().getName(), equalTo(user.getName()));
    assertThat(response.getBody().getId(), equalTo(user.getId()));
    assertThat(response.getBody().getRole(), equalTo(user.getRole()));

  }

  @Test
  public void testLoginFailure() {

    ResponseEntity<String> response = post("/login").formParam("username", "1").formParam("password", "2")
        .getResponse(String.class);

    assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN));
  }

  @Test
  public void testUnauthorized() {
    get("/me").expectedStatus(HttpStatus.UNAUTHORIZED).getResponse();
  }
}
