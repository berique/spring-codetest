package io.redspark.controller;

import io.redspark.MailApplicationTest;
import io.redspark.domain.User;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static io.redspark.compose.Compose.admin;

public class MailControllerTest extends MailApplicationTest {

    @Test
    public void sendMailTest() {
        User caio = admin("caio");
        saveall(caio);
        signIn(caio);

        get("/send-mail").queryParam("to", "caio.ferreira@dclick.com.br").expectedStatus(HttpStatus.OK).getResponse();
    }
}