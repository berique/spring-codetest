package io.redspark.compose;

import io.redspark.domain.Client;
import io.redspark.domain.User;
import io.redspark.domain.Vet;
import io.redspark.security.Roles;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class Compose {

    public static User admin(String name) {
        return new User(null, name, name, name);
    }

    public static Vet vet(String name) {
        return Vet.builder().login(name).password(name).name(name).build();
    }

    public static Client client(String name, String email) {
        return Client.builder().login(name).email(email).name(name).build();
    }

    @Test
    public void testAdmin() {
        assertThat(admin("john").getRole(), is(Roles.ROLE_ADMIN));
    }

    @Test
    public void testClient() {
        assertThat(client("john", "john@rocket.com").getRole(), is(Roles.ROLE_CLIENT));
    }

    @Test
    public void testVet() {
        assertThat(vet("john").getRole(), is(Roles.ROLE_VET));
    }
}
