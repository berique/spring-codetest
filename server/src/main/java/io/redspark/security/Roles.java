package io.redspark.security;

public class Roles {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_VET = "ROLE_VET";
    public static final String ROLE_CLIENT = "ROLE_CLIENT";

}
