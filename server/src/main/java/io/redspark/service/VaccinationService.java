package io.redspark.service;

import io.redspark.domain.Client;
import io.redspark.domain.Vaccination;
import io.redspark.repository.VaccinationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class VaccinationService {

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    private final SimpleDateFormat sdfTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Autowired
    private SmtpMailSender smtpMailSender;

    @Autowired
    private VaccinationRepository vaccinationRepository;

    public String createBody(Vaccination vaccination, Client client) {
        return String.format("Olá %1s,\n\nO(a) %2s precisa tomar a vacina \"%3s\".", client.getName(), vaccination.getPet().getName(), vaccination.getName());
    }

    public String createSubject(Vaccination vaccination) {
        return String.format("Vaccination: %1s", vaccination.getName());
    }

    public void checkVaccinationAndSendMail() throws ParseException, MessagingException {
        checkVaccinationAndSendMail(new Date());
    }

    public void checkVaccinationAndSendMail(Date parse) throws ParseException, MessagingException {
        String date = sdf.format(parse);
        List<Vaccination> vaccinationList = vaccinationRepository.findByVaccinationDateBetween(
                sdfTime.parse(date + " 00:00:00"),
                sdfTime.parse(date + " 23:59:59")
        );
        for (Vaccination vaccination : vaccinationList) {
            final Client client = vaccination.getPet().getClient();

            final String subject = createSubject(vaccination);

            final String body = createBody(vaccination, client);
            smtpMailSender.send(client.getEmail(), subject, body);
        }
    }
}
