package io.redspark.repository;

import io.redspark.domain.Vaccination;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface VaccinationRepository extends JpaRepository<Vaccination, Long> {

    List<Vaccination> findByVaccinationDateBetween(Date start, Date end);
}
