package io.redspark.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="appointment")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Appointment {
    @Id
    @GeneratedValue
    @Column(name = "appointment_id")
    private Long id;

    private Date appointmentDate = new Date();

    @ManyToOne
    @JoinColumn(name = "appointement_vet_id")
    private Vet vet;

    @ManyToOne
    @JoinColumn(name = "appointement_pet_id")
    private Pet pet;
}
