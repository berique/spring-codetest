package io.redspark.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "vaccination")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Vaccination {
    @Id
    @GeneratedValue
    @Column(name = "vaccination_id")
    private Long id;

    @Column(name = "vaccination_name")
    private String name;

    @Column(name = "vaccination_date")
    private Date vaccinationDate;

    @ManyToOne
    @JoinColumn(name = "vaccination_vet_id")
    private Vet vet;

    @ManyToOne
    @JoinColumn(name = "vaccination_pet_id")
    private Pet pet;
}
