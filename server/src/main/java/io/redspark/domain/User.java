package io.redspark.domain;

import io.redspark.security.Roles;
import io.redspark.security.UserAuthentication;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "user", uniqueConstraints = @UniqueConstraint(name = "UK_LOGIN", columnNames = "user_login"))
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements UserAuthentication {

    @Column(name = "user_role")
    @Setter(AccessLevel.NONE)
    protected final String role = Roles.ROLE_ADMIN;

    @GeneratedValue
    @Id
    @Column(name = "user_id")
    private Long id;

    @Column(name = "user_login", nullable = false, updatable = false)
    private String login;

    @Column(name = "user_name", nullable = false)
    private String name;

    @Column(name = "user_password", nullable = false)
    private String password;

}
