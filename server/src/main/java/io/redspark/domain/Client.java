package io.redspark.domain;

import io.redspark.security.Roles;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@NoArgsConstructor
public class Client extends User {

    protected final String role = Roles.ROLE_CLIENT;

    @Column(name = "client_email", unique = true)
    private String email;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Pet> pets;

    @Builder
    public Client(Long id, String login, String name, String password, String email, List<Pet> pets) {
        super(id, login, name, password);
        this.email = email;
        this.pets = pets;
    }
}
