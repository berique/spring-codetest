package io.redspark.domain;

import io.redspark.security.Roles;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@NoArgsConstructor
public class Vet extends User {
    protected final String role = Roles.ROLE_VET;

    @Builder
    public Vet(Long id, String login, String name, String password) {
        super(id, login, name, password);
    }

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Appointment> appointments;
}
