package io.redspark;

import io.redspark.service.VaccinationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.mail.MessagingException;
import java.text.ParseException;


@Configuration
@EnableScheduling
public class SchedulingConfig {

    private static final Logger log = LoggerFactory.getLogger(SchedulingConfig.class);
    @Autowired
    private VaccinationService vaccinationService;

    @Scheduled(cron = "0 0 * * * *")
    public void checkVaccinationAndSendEmail() {
        try {
            vaccinationService.checkVaccinationAndSendMail();
        } catch (ParseException e) {
            log.error(e.getMessage());
        } catch (MessagingException e) {
            log.error(e.getMessage());
        }
    }
}
