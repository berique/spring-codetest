package io.redspark.controller.dto;

import io.redspark.security.DefaultUser;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO {

  private Long id;
  private String name;
  private String role;

  public UserDTO(DefaultUser user) {
    this.id = user.getId();
    this.name = user.getName();
    this.role = user.getAuthorities().get(0).getAuthority();
  }
}