# Proposta de solução

## Dominios

[x] Criar entidade `io.redspark.domain.Pet` para o cadastro de animais que são tratados pela clinica, ele conterá o nome nome do "pet" e o dono do "pet" (cliente), consultas e vacinas.

[x] Criar entidade `io.redspark.domain.Appointment` para as consultas que conterá a data e hora da consulta, o veterinario e o "pet".

[x] Criar entidade `io.redspark.domain.Vaccination` para as vacinas dos animais, que conterá o nome da vacina, data da vacinação, a consulta e o "pet". Se a data da vacinação for maior que a data consulta então é pré-agendada. 

[x] Criar entidade `io.redspark.domain.Client` que terá propriedade e-mail e uma list de "pets" ligado a ele, além do papel de cliente.

[x] Criar entidade `io.redspark.domain.Vet` que terá que terá a lista consultas.

[x] Adicionar propiedade `role` como String nas classes: `io.redspark.domain.User` e `io.redspark.controller.dto.UserDTO`. Assim adicionando o role `vet` para os veterinários e `client` para cliente.


## Testes

### Cliente

[x] Insert do cliente com Pet

[x] Insert do cliente sem Pet

[x] Atualizaçao do nome do cliente com Pet

[x] Atualizaçao do nome do cliente sem Pet

[x] Remoção do cliente do cliente com Pet e do Pet.

[x] Remoção do cliente do cliente sem Pet.

### Pet

[x] Inserir Pet sem cliente

[x] Inserir Pet com cliente

[x] Inserir Pet com cliente e com vacina

[x] Atualizar o nome do Pet

[x] Remover o Pet com cliente e o cliente permanece

### Consulta

[ ] Inserir consulta

### Vacina

[x] Inserir vacina

[x] Query que irá trazer os Pet que estão com a vacina marcada para a data especifica.


## Schedule 

[x] Hábilitar schedule no spring boot e utilizar _expessão cron_ `0 0 * * * *`. Assim verificando todos os dias a meia-noite checando todas as vanicaçõs pré-agendadas para aquela data e enviará e-mail aos clientes.



# WhishList

## Sistema de template

Utilizar um sistema de template para o e-mail

## Melhorar o Schedule

Criar uma tabela só para o envio de e-mail, assim se o veterinário fizer o cadastro de vacina na hora do schedule o e-mail não será enviado.

## Repositórios como RestRepository

Colocar todos os repository como `@RepositoryRestResource`, possibilitando assim preenchimento via REST.

## E-mail para atendimento

O atendimento também poderia ser agendado.